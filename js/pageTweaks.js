/* 
   pageTweaks.js
   Copyright (C) 2005 Deryck Hodge <deryck@samba.org>

   JS functions for tweaking page styles, forms, etc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

function changeMirror(mirrorChoice)
{
	mirrorSite = mirrorChoice.options[mirrorChoice.selectedIndex].value;

	if (mirrorSite != "") {
		document.location.href = mirrorSite;
	}
}

function setContentLen()
{
	var content = document.getElementById('content');
	var sidebar;
	var contentY;
	var sidebarY;

	if (document.getElementById('links')) {
		sidebar = document.getElementById('links');
	} else if (document.getElementById('countries')) {
		sidebar = document.getElementById('countries'); 
	} else if (document.getElementById('nav')) {
		sidebar = document.getElementById('nav');
	}

	// Get the sidebar height as integer
	if (is_ie) {
		contentY = content.offsetHeight;
		sidebarY = sidebar.offsetHeight;
	} else {
		contentY = window.getComputedStyle(content, '').getPropertyValue('height');
		sidebarY = window.getComputedStyle(sidebar, '').getPropertyValue('height');
		contentY = parseInt(contentY.slice(0,-2));
		sidebarY = parseInt(sidebarY.slice(0,-2));
	}

	// Allow for the extra top margin value
	if (document.getElementById('links') || 
		    document.getElementById('nav')) {
		sidebarY = sidebarY + 115;
	} else if (document.getElementById('countries')) {
		sidebarY = sidebarY + 240; 
	}
	
	if (sidebarY + 50 > contentY) {
		content.style.height = sidebarY.toString() + 'px';
	}
}
