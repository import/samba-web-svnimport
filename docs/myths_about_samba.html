<!--#include virtual="/samba/header.html" -->
  <title>Myths About Samba</title>
<!--#include virtual="/samba/header_wide.html" -->

    <h3><b>Myths About Samba</b></h3>

    <p><b>~by Andrew Tridgell</b></p>

    <p>Originally published on <a href="http://www.groklaw.net/">Groklaw</a>.</p>

    <p>There have been a number of press reports lately that discuss the
    techniques used in developing Samba, and that refer to those techniques as
    "reverse engineering". That term is quite misleading, and does not at all
    accurately reflect the techniques that the Samba Team actually use. I
    think that it is time to describe again the techniques that we actually
    use, and to dispel some other common myths about Samba development.<br>
    <br>
    </p>

    <p><b><u>Myth 1: dissecting the waiter</u></b></p>

    <p>Classical reverse engineering techniques in software engineering
    revolve around the use of disassemblers, debuggers and other object code
    analysis tools to examine directly the executable code of an existing
    product in order to create a "clone" that behaves in the same way. While
    these venerable techniques have been successfully used by many groups,
    they are not what we use in the Samba project.</p>

    <p>Apart from the fact that these techniques often lead to very poor code
    in a new implementation, in the Samba Team we are very conservative when
    it comes to techniques that might be legally controversial.</p>

    <p>For people who aren't software engineers I usually use the "French
    Cafe" analogy to describe the techniques that are really used in the
    development of Samba. You can see the original French Cafe description at:
    <a href="http://samba.org/ftp/tridge/misc/french_cafe.txt">http://samba.org/ftp/tridge/misc/french_cafe.txt</a></p>

    <p>I suggest you go and read the French Cafe description now, then come
    back, as the rest of this paper won't really make any sense until you have
    read it.</p>

    <p>I originally wrote that paper as part of a submission made by the Samba
    Team to the European Commission Microsoft anti-trust trial. The aim was to
    make the techniques used by the Samba Team more easily understood by
    judges who might not be familiar with software engineering techniques.</p>

    <p>Extending that analogy a little, I think it would be fair to say that
    classical reverse engineering techniques would be like taking a scalpel
    and dissecting the waiter. It might indeed be possible to find out how the
    waiter "works" from a biological perspective by using a scalpel, but it is
    also possible to find out what you need to know by just talking to the
    waiter, as long as you are persistent. The Samba Team has been "talking to
    the waiter" for thirteen years now, and it might be fair to say that we
    know more about his job than he does himself.</p>

    <p>For future reference, the terms we usually use to describe what we do
    in developing Samba are "network analysis" or "protocol analysis". If you
    see someone describing Samba as "reverse engineering" then please ask them
    to use these terms instead, or point them at this paper.</p>

    <p><u><b>Myth 2: which came first?</b></u></p>

    <p>Many people incorrectly see Samba as being a "clone" of WindowsNT. A
    quick check of history will confirm that the very idea is absurd. I first
    released Samba in January 1992, whereas the first release of WindowsNT was
    in August 1993. Unless you also credit me with inventing time travel, it
    is difficult to see how I could have been aiming to produce a "clone" of
    WindowsNT.</p>

    <p>Furthermore, I didn't find out that Samba inter-operated at all with
    any version of DOS or Windows until nearly two years after I did that
    first release. The earliest versions of Samba were aimed at being
    compatible with clients for the Digital Equipment Corporation "Pathworks"
    product suite, which was an early implementation of the SMB protocol that
    was quite popular at the time, and that ran on Ultrix and VMS systems. My
    initial motivation was to implement the same protocol on SunOS, so that I
    could use my desktop system to access the departmental Unix server.</p>

    <p>These days we obviously take great care to ensure that Samba is as
    compatible as possible with current Microsoft Windows clients. That is
    absolutely essential, as their monopoly market position in desktop
    operating systems means that Samba is used with Microsoft clients more
    than any other client. That doesn't mean we ignore other clients that
    implement the SMB protocol, it just means that we test against Microsoft
    clients more often than we test against other clients.</p>

    <p><u><b>Myth 3: who invented CIFS?</b></u></p>

    <p>Another common myth is to assume that Microsoft "invented" the
    protocols that Samba implements. To understand how wrong this is you need
    to know a little bit of history.</p>

    <p>The protocol that Samba implements was first invented by Barry
    Feigenbaum at IBM in early 1983. He initially called it the "BAF" protocol
    after his initials, but changed the name to "SMB" before the first
    official release. You may note that the name "Samba" contains the letters
    "SMB", and that is not a coincidence.</p>

    <p>The term "CIFS" or "Common Internet File System" was coined by
    Microsoft in 1996 as a marketing exercise in an attempt to combat a
    perceived threat from Sun Microsystems after their WebNFS announcement.
    The term caught on, and now the SMB protocol is often called CIFS. The two
    names refer to the same protocol, as is easily demonstrated by connecting
    a current Microsoft "CIFS" client to a Samba "SMB" server from 1992.</p>

    <p>It should also be noted that SMB/CIFS is an evolving protocol. The
    original design by Barry Feigenbaum deliberately allowed scope for
    protocol extensions, and a number of groups have taken advantage of this
    over the years. The largest set of extensions have been made by Microsoft,
    but some quite substantial extensions have been made by other groups,
    including SCO, Thursby, IBM, Apple and the Samba Team.</p>

    <p>It might be worth noting that of all these extensions, only Microsoft
    has kept some (but not all) of their additions secret. It is also
    important to note that in many cases Microsoft clients do not operate
    correctly with servers that do not implement the Microsoft extensions.
    That is, fundamentally, why we need to use the "network analysis"
    techniques described above.</p>

    <p>From 1996 until about five years ago Microsoft played a leading role in
    the effort to standardize the SMB/CIFS protocol. They started the annual
    CIFS conference series that continues today, they started the effort to
    create an IETF standard for the protocol and they ran a public discussion
    list on all aspects of the protocol. For reasons that we still don't fully
    understand, they have since withdrawn from all of these activities, and
    now appear to be actively hostile to open standards efforts.</p>

    <p>Despite this change of heart on behalf of the biggest player, the CIFS
    conferences and plug-fests have continued with the help of the Storage
    Network Industry Association, and with the Samba Team playing a leading
    technical role, alongside industry heavyweights such as Network Appliance
    and EMC.</p>

    <p><u><b>Where to now?</b></u></p>

    <p>I still hold out hope that Microsoft will someday rejoin the SMB/CIFS
    developer community by attending the annual conference and plug-fest. A
    lot has happened since they stopped attending, including the development
    of a comprehensive test suite that covers most of the protocol. Microsoft
    is doing its customers a great disservice by ignoring the opportunity to
    work with other industry members, and could certainly benefit from
    attending the plug-fest and learning about the extensive protocol test
    suites that are now available.</p>

    <p>If anyone from Microsoft is reading this, then I encourage you to look
    at <a href="http://www.cifs2005.org/">http://www.cifs2005.org/</a> and
    seriously think about sending someone with the technical knowledge to
    discuss what has been happening in the last few years.</p>
    <hr>

    <p>Andrew Tridgell (tridge at osdl.org)</p>


<!--#include virtual="/samba/footer.html" -->
